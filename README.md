## Signature Field

Signature Field allows you to collect HTML5 canvas based signatures in content
type. Currently it displays the collected signature as an image and in the back
end it stores it as an BLOB.


 * For a full description of the module visit:
   https://www.drupal.org/project/signature_field
 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/signature_field

### Requirements

This module requires no modules outside of Drupal core.

Requires Signature Pad JS library:
https://github.com/szimek/signature_pad/

### Install

Install the Signature Field module as you would normally install a contributed
Drupal module.

Visit https://www.drupal.org/node/1897420 for further
information.

### Configuration

  1. Install this module.
  2. Go to Manage fields page of your content types.
  3. Create or alter existing Signature field on your content type.
  4. Configure field as needed and save.
  5. You should now be able to capture signatures.

### Maintainers

Current Maintainers:
 * George Anderson - https://www.drupal.org/u/geoanders
 * Saranya Purushothaman - https://www.drupal.org/u/saranya-purushothaman
